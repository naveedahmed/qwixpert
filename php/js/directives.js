angular
    .module('Qwixpert')
    .directive('slider', [function() {
        return {
            restrict: 'A',
            link: function(scope, elem, attrs) {
                $(elem).owlCarousel({
                    margin: 0,
                    nav: true,
                    autoplay: true,
                    dots: false,
                    navText: [
                        "<span class='lnr lnr-chevron-left'></span>",
                        "<span class='lnr lnr-chevron-right'></span>"
                    ],
                    items: 1
                });
            }
        };
    }])
    .directive('tab', [function() {
        return {
            restrict: 'A',
            link: function(scope, elem, attrs) {
                $(elem).on("click", function() {
                	//reset
                	 $('.nav-tabs li, .tab-content .tab-pane').removeClass('active');
                	 $(this).addClass('active');
                     $(attrs.target).addClass('active');
                });
            }
        };
    }]);
