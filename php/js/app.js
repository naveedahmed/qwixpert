 /**
  * You must include the dependency on 'ngMaterial' 
  */
 angular.module('Qwixpert', ['ngMaterial', 'ngRoute', 'ui.bootstrap', 'ngAnimate'])
     .config(function($routeProvider) {
         $routeProvider
             .when("/", {
                 templateUrl: "templates/home.html",
                 controller: 'HomeCtrl'
             })
             .when("/contact", {
                 templateUrl: "templates/contact.html",
                 controller: 'ContactCtrl'
             })
             .when("/our-experts", {
                 templateUrl: "templates/our-experts.html",
                 controller: 'ExpertsCtrl'
             })
             .when("/why-qwixpert", {
                 templateUrl: "templates/why-qwixpert.html",
                 controller: 'WhyqwixpertCtrl'
             })
             .when("/trending-topics", {
                 templateUrl: "templates/trending-topics.html",
                 controller: 'TrendingCtrl'
             })
             .when("/how-it-works", {
                 templateUrl: "templates/how-it-works.html",
                 controller: 'HowItWorksCtrl'
             })
             .when("/get-started", {
                 templateUrl: "templates/get-started.html",
                 controller: 'GetStartedCtrl'
             })
             .when("/terms", {
                 templateUrl: "templates/terms.html",
                 controller: 'TermsCtrl'
             })
             .when("/privacy-policy", {
                 templateUrl: "templates/privacy-policy.html",
                 controller: 'PrivacyPolicyCtrl'
             })
             .when("/business-type/startup", {
                 templateUrl: "templates/business-type/startup.html",
                 controller: 'BusinessTypeCtrl'
             })
             .when("/business-type/small-business", {
                 templateUrl: "templates/business-type/small-business.html",
                 controller: 'BusinessTypeCtrl'
             })
             .when("/business-type/medium-business", {
                 templateUrl: "templates/business-type/medium-business.html",
                 controller: 'BusinessTypeCtrl'
             })
             .when("/business-type/enterprise-client", {
                 templateUrl: "templates/business-type/enterprise-client.html",
                 controller: 'BusinessTypeCtrl'
             })
             .when("/business-type/consulting-firm", {
                 templateUrl: "templates/business-type/consulting-firm.html",
                 controller: 'BusinessTypeCtrl'
             })
             .when("/business-type/financial-institution", {
                 templateUrl: "templates/business-type/financial-institution.html",
                 controller: 'BusinessTypeCtrl'
             })
             .otherwise('/');
     });;
