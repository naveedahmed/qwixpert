angular
    .module('Qwixpert')
    .controller('AppCtrl', function($scope, Page) {
        $scope.page = Page;
        console.log($scope.page.title());


    })
    .controller('HomeCtrl', function($scope, Page) {
        $scope.changeTab = function(tab) {
            $scope.view_tab = tab;
            console.log($scope.view_tab);
        }


    })
    .controller('ContactCtrl', function($scope, backendService, $mdDialog) {
        $scope.data = { type: "contact" };
        $scope.message = message;

        function message() {
            var request = backendService.submit($scope.data);
            request.then(function(response) {
                if (true) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.querySelector('.expert-login')))
                        .clickOutsideToClose(true)
                        .title('Thank you for contacting us!')
                        .textContent('Your message has been sent successfully.')
                        .ariaLabel('Qwixpert')
                        .ok('Okay')
                    );
                $scope.data={};
                }
            });
        }

    })
    .controller('ExpertsCtrl', function($scope) {

    })
    .controller('WhyqwixpertCtrl', function($scope) {

    })
    .controller('TrendingCtrl', function($scope) {

    })
    .controller('HowItWorksCtrl', function($scope) {

    })
    .controller('GetStartedCtrl', function($scope, $mdDialog, backendService) {
        $scope.business = {};
        $scope.consultant = {};

        $scope.tab = 'business';

        $scope.changeTab = changeTab;
        $scope.registerBusiness = registerBusiness;
        $scope.registerConsultant = registerConsultant;

        function changeTab(tab) {
            $scope.tab = tab;
        }

        function registerBusiness() {
            $scope.business.type = "business";
            var request = backendService.submit($scope.business);
            request.then(function(response) {
                if (true) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.querySelector('.expert-login')))
                        .clickOutsideToClose(true)
                        .title('Thank you for signing up with us!')
                        .textContent('We will get back to you shortly.')
                        .ariaLabel('Qwixpert')
                        .ok('Okay')
                    );
                    $scope.business = {};
                }
            });
        }

        function registerConsultant() {
            $scope.consultant.type = "consultant";
            var request = backendService.submit($scope.consultant);
            request.then(function(response) {
                if (true) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.querySelector('.expert-login')))
                        .clickOutsideToClose(true)
                        .title('Thank you for signing up with us!')
                        .textContent('We will get back to you shortly.')
                        .ariaLabel('Qwixpert')
                        .ok('Okay')
                    );
                    $scope.consulatant = {};

                }
            });
        }


    })
    .controller('TermsCtrl', function($scope) {

    })
    .controller('PrivacyPolicyCtrl', function($scope) {

    })
    .controller('BusinessTypeCtrl', function($scope) {

    });
