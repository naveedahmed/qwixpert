angular
    .module('Qwixpert')
    .factory('Page', function() {
        var title = 'Qwixpert';
        return {
            title: function() {
                return title;
            },
            setTitle: function(newTitle) { title = newTitle }
        };
    }).directive("fixed", function($window) {

        return {
            restrict: "EA",
            scope: false,
            link: function(scope, elem, attr) {

                angular.element($window).bind("scroll", function() {
                    if (this.pageYOffset >= 100) {
                        scope.isFixed = true;
                    } else {
                        scope.isFixed = false;
                    }
                    scope.$apply();

                });
            }
        };

    })
    .directive("counter", function() {
        return {
            restrict: "EA",
            scope: false,
            link: function(scope, elem, attr) {
                console.log(elem);
                angular.element(elem).counterUp({
                    delay: 10,
                    time: 1000
                });
            }
        }
    })
    .factory("backendService", function($http) {
        return {
            submit: function($data) {
                return $http({
                    method: 'POST',
                    url: '/aws-mail.php',
                    data: $.param($data), // pass in data as strings
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
                }).success(function(data) {
                    console.log('success');
                }).error(function(data) {
                    console.log('error');
                });
            }

        }
    });
